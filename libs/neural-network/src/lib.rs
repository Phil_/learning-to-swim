#![allow(dead_code)]
use rand::RngCore;
use rand::prelude::*;

#[macro_use]
extern crate derivative;

#[derive(Clone, Debug)]
struct Neuron {
    bias: f64,
    weights: Vec<f64>,
}


impl Neuron {
    fn random(rng: &mut dyn RngCore, output_size: usize) -> Self {
        let bias = rng.gen_range(-1.0..=1.0);

        let weights = (0..output_size)
            .map(|_| rng.gen_range(-1.0..=1.0))
            .collect();

        Self { bias, weights }
    }

    fn propagate(&self, input: &[f64]) -> f64 {
        assert_eq!(input.len(), self.weights.len());
        self.bias + self.weights.iter().zip(input.iter()).map(|(w, i)| w * i).sum::<f64>()
    }
}

impl Default for Neuron {
    fn default() -> Self {
        Self {
            bias: 0.0,
            weights: vec![0.]
        }
    }
}

#[derive(Derivative)]
#[derivative(Debug)]
struct Layer {
    neurons: Vec<Neuron>,
    #[derivative(Debug="ignore")]
    activation: Option<Box<dyn Fn(f64) -> f64>>,
}

impl Layer {
    fn random(rng: &mut dyn RngCore, input_size: usize, output_size: usize) -> Self {
        let neurons = (0..output_size)
            .map(|_| Neuron::random(rng, input_size))
            .collect();

        Self { neurons, activation: None }
    }

    fn propagate(&self, input: &[f64]) -> Vec<f64> {
        let out: Vec<f64> = self.neurons.iter()
            .map(|neuron| neuron.propagate(input))
            .collect();

        if let Some(fn_box) = &self.activation {
            out.iter()
                .map(|val| fn_box(*val))
                .collect()
        } else {
            out
        }
    }
}

impl Default for Layer {
    fn default() -> Self {
        Self {
            neurons: vec![
                Neuron::default(),
            ],
            activation: None
        }
    }
}

pub struct LayerTopology {
    neurons: usize,
}

#[derive(Debug)]
pub struct Network {
    layers: Vec<Layer>,
}

impl Network {
    pub fn random(rng: &mut dyn RngCore, layer_sizes: &[LayerTopology]) -> Self {
        let layers = layer_sizes
            .windows(2)
            .map(|layers| {
                Layer::random(rng, layers[0].neurons, layers[1].neurons)
            })
        .collect();

        Self { layers }
    }

    pub fn propagate(&self, input: &[f64]) -> Vec<f64> {
        let mut layer_out = input.to_owned();
        for layer in self.layers.iter() {
            layer_out = layer.propagate(&layer_out);
        }

        layer_out
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use rand_pcg::Pcg64;

    fn get_rng() -> Pcg64 {
        Pcg64::seed_from_u64(42)
    }

    mod neuron {
        use super::*;

        #[test]
        fn test_new() {
            let mut rng = get_rng();

            let n = Neuron::random(&mut rng, 2);

            let foo = Neuron {
                bias: -0.5469749641433731,
                weights: vec![-0.521786619362801, 0.5216668213734823],
            };

            approx::assert_relative_eq!(n.bias, foo.bias);
            approx::assert_relative_eq!(n.weights.as_slice(), foo.weights.as_slice());
        }

        #[test]
        fn test_propagate() {
            let n = Neuron {
                bias: -1.,
                weights: vec![0.4, -0.3],
            };

            let out = n.propagate(&[0., 1.]);

            approx::assert_relative_eq!(out, -1.3);
        }
    }

    mod layer {
        use super::*;

        #[test]
        fn test_new() {
            let mut rng = get_rng();

            let n = Layer::random(&mut rng, 2, 2);
            let foo = Layer {
                neurons: vec![
                    Neuron {
                        bias: -0.5469749641433731,
                        weights: vec![-0.521786619362801, 0.5216668213734823]
                    },
                    Neuron {
                        bias: 0.055679314716303496,
                        weights: vec![0.9409526354818318, -0.2490746371709609]
                    }
                ],
                ..Default::default()
            };

            approx::assert_relative_eq!(n.neurons[0].bias, foo.neurons[0].bias);
            approx::assert_relative_eq!(n.neurons[0].weights.as_slice(), foo.neurons[0].weights.as_slice());
            approx::assert_relative_eq!(n.neurons[1].bias, foo.neurons[1].bias);
            approx::assert_relative_eq!(n.neurons[1].weights.as_slice(), foo.neurons[1].weights.as_slice());
        }

        #[test]
        fn test_propagate() {
            let l = Layer {
                neurons: vec![
                    Neuron {
                        bias: 0.3,
                        weights: vec![0.0, 0.1, 0.2],
                    },
                    Neuron {
                        bias: 0.5,
                        weights: vec![0.1, 0.1, 0.6],
                    }
                ],
                ..Default::default()
            };

            let out = l.propagate(&[1., 2., 3.]);
            let should = vec![1.1, 2.6];

            approx::assert_relative_eq!(out.as_slice(), should.as_slice());
        }

        #[test]
        fn test_activation() {
            let l = Layer {
                neurons: vec![
                    Neuron {
                        bias: 0.3,
                        weights: vec![0.0, 0.1, 0.2],
                    },
                    Neuron {
                        bias: 0.5,
                        weights: vec![0.1, 0.1, 0.6],
                    }
                ],
                activation: Some(Box::new(|v| 1.0 / (1.0 + f64::exp(-v)) ))
            };

            let out = l.propagate(&[1., 2., 3.]);
            let should = vec![0.7502601055951177, 0.9308615796566531];

            approx::assert_relative_eq!(out.as_slice(), should.as_slice());

        }
    }

    mod network {
        use super::*;

        #[test]
        fn test_new() {
            let mut rng = Pcg64::seed_from_u64(42);
            let n = Network::random(&mut rng, &[
                LayerTopology { neurons: 4 },
                LayerTopology { neurons: 1 },
            ]);

            let foo = Network {
                layers: vec![
                    Layer {
                        neurons: vec![
                            Neuron {
                                bias: -0.5469749641433731,
                                weights: vec![-0.521786619362801, 0.5216668213734823, 0.055679314716303496, 0.9409526354818318]
                            }
                        ],
                        ..Default::default()
                    }
                ]
            };

            approx::assert_relative_eq!(n.layers[0].neurons[0].bias, foo.layers[0].neurons[0].bias);
            approx::assert_relative_eq!(n.layers[0].neurons[0].weights.as_slice(), foo.layers[0].neurons[0].weights.as_slice());
        }

        #[test]
        fn test_propagate() {
            let n = Network {
                layers: vec![
                    Layer {
                        neurons: vec![
                            Neuron {
                                bias: 0.1,
                                weights: vec![0.1, 0.2, 0.3, 0.4],
                            },
                            Neuron {
                                bias: 0.1,
                                weights: vec![0.1, 0.2, 0.3, 0.4],
                            },
                        ],
                        ..Default::default()
                    },
                    Layer {
                        neurons: vec![
                            Neuron {
                                bias: 0.1,
                                weights: vec![0.3, 0.4],
                            },
                        ],
                        ..Default::default()
                    },
                ]
            };

            let out = n.propagate(&[0.7, 0.1, 0.1, 0.5]);
            let should = vec![0.394];

            approx::assert_relative_eq!(out.as_slice(), should.as_slice());
        }
    }
}
