#![allow(dead_code)]

use num::{
    Float,
    clamp
};
use rand::prelude::*;
use rand::distributions::Standard;
use rand::RngCore;


#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Position<T> {
    x: T,
    y: T,
}

impl<T: Float> Position<T>
where Standard: Distribution<T>,
      f64: Into<T> {
    fn new(x: T, y: T) -> Self {
        let min = (0.).into();
        let max = (1.).into();

        let x = clamp(x, min, max);
        let y = clamp(y, min, max);

        Self { x, y }
    }

    pub fn random(rng: &mut dyn rand::RngCore) -> Self {
        let (x, y);

        x = rng.gen::<T>();
        y = rng.gen::<T>();

        Self { x, y }
    }

    pub fn update(&mut self, dx: f64, dy: f64) {
        let dx = dx.into();
        let dy = dy.into();

        let min = (0.).into();
        let max = (1.).into();

        self.x = clamp(self.x + dx, min, max);
        self.y = clamp(self.y + dy, min, max);
    }
}

impl<T> Default for Position<T>
where f64: Into<T> {
    fn default() -> Self {
        let x = (0.5).into();
        let y = (0.5).into();

        Self { x, y }
    }
}

impl<T> Into<(T, T)> for Position<T> {
    fn into(self) -> (T, T) {
        (self.x, self.y)
    }
}

#[derive(Debug)]
pub struct Boid {
    pos: Position<f64>,
    direction: f64,
    speed: f64,
    nourishment: f64,
    ticks_alive: u64,
}


impl Boid {
    pub fn random(mut rng: &mut dyn RngCore) -> Self {
        let direction = rng.gen();
        let speed = rng.gen();
        let nourishment = rng.gen();

        let pos = Position::random(&mut rng);

        Self {
            pos,
            direction,
            speed,
            nourishment,
            ..Default::default()
        }
    }

    pub fn step(&mut self) {
        let (x, y) = self.pos.into();

        let dx = self.speed * x.cos();
        let dy = self.speed * y.sin();

        self.pos.update(dx, dy);
    }
}


impl Default for Boid {
    fn default() -> Self {
        let pos = Position::default();

        Self {
            pos,
            direction: 0.,
            speed: 0.5,
            nourishment: 0.5,
            ticks_alive: 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod position {
        use super::*;

        #[test]
        fn test_into() {
            let pos = Position::new(0.56, 0.2);
            let (x, y) = pos.into();

            assert_eq!(x, 0.56);
            assert_eq!(y, 0.2);
        }

        #[test]
        fn test_clamp() {
            // x upper
            let pos = Position::new(1.1, 0.5);
            let (x, _) = pos.into();
            assert_eq!(x, 1.);

            // x lower
            let pos = Position::new(-1.1, 0.5);
            let (x, _) = pos.into();
            assert_eq!(x, 0.);

            // y upper
            let pos = Position::new(0.5, 1.5);
            let (_, y) = pos.into();
            assert_eq!(y, 1.);

            // y lower
            let pos = Position::new(0.5, -1.5);
            let (_, y) = pos.into();
            assert_eq!(y, 0.);
        }

        #[test]
        fn test_update() {
            let mut old = Position::new(0.5, 0.5);
            old.update(0.2, 0.3);

            let new = Position::new(0.7, 0.8);

            assert_eq!(old, new);
        }

        #[test]
        fn test_update_clamp() {
            let mut old = Position::new(0.5, 0.5);
            old.update(1.5, -2.3);

            let new = Position::new(1., 0.);

            assert_eq!(old, new);
        }
    }

    mod boid {
        use super::*;

        #[test]
        fn test_random() {
            let nit = 1000;

            let mut rng = rand::thread_rng();

            for _ in 1..nit {
                let boid = Boid::random(&mut rng);
                assert!(boid.direction < 1. && boid.direction >= 0.);
                assert!(boid.speed < 1. && boid.speed >= 0.);
                assert!(boid.nourishment < 1. && boid.nourishment >= 0.);

                let (x, y) = boid.pos.into();

                assert!(x < 1. && x >= 0.);
                assert!(y < 1. && y >= 0.);
            }
        }
    }

}
