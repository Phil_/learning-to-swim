{
  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, fenix, flake-utils, naersk, nixpkgs }:
  flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ fenix.overlay ];
    };

    # env + toolchain
    rustEnv = fenix.packages.${system}.latest;
    toolchain = rustEnv.toolchain;

    naersk-lib = naersk.lib.${system}.override {
      cargo = toolchain;
      rustc = toolchain;
      #inherit (toolchain) cargo rustc;
    };
  in {
    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [
        toolchain
      ];
    };

    defaultPackage = naersk-lib.buildPackage {
      release = false;
      src = ./.;
    };
  });
}
